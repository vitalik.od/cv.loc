<?php

include_once $_SERVER['DOCUMENT_ROOT'] . '/admin/works/WorksRepository.php';

$workRepository = new WorksRepository();
$works = $workRepository->getAll();

?>

<!DOCTYPE html>
<html>
<head>
    <title>admin</title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
</head>

<body>
<nav class="navbar navbar-expand-lg navbar-light bg-light">
    <div class="collapse navbar-collapse" id="navbarNav">
        <ul class="navbar-nav">
            <li class="nav-item">
                <a class="nav-link" href="/admin/">Home</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="/admin/skills/pro">Prof skill</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="/admin/skills/per">Personal skill</a>
            </li>
            <li class="nav-item active">
                <a class="nav-link" href="/admin/works">Works</a>
            </li>
        </ul>
    </div>
</nav>
<table class="table table-dark">
    <thead>
    <tr>
        <th scope="col">#</th>
        <th scope="col">Company</th>
        <th scope="col">Start Date</th>
        <th scope="col">End Date</th>
        <th scope="col">Web Site</th>
        <th scope="col">Details</th>
        <th scope="col"><a href="/admin/works/formInsert.php">Add</a></th>
    </tr>
    </thead>
    <tbody>
    <?php foreach ($works as $work): ?>
    <tr>
        <th scope="row"><?=$work['id']?></th>
        <td><?=$work['title']?></td>
        <td><?=$work['start_date']?></td>
        <td><?=$work['end_date']?></td>
        <td><?=$work['url']?></td>
        <td><?=$work['details']?></td>
        <td><a href="/admin/works/formUpdate.php?id=<?=$work['id']?>">Edit</a></td>
        <td><a href="/admin/works/delete.php?id=<?=$work['id']?>">Delete</a></td>
    </tr>
    <?php endforeach; ?>
    </tbody>
</table>
</body>

</html>
