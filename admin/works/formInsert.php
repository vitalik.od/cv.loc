<?php
?>

<!DOCTYPE html>
<html>
<head>
    <title>admin</title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
</head>

<body>
<div class="container">
    <form action="./insert.php" method="post">
        <div class="form-group">
            <label for="exampleFormControlInput1">Skill</label>
            <input type="text" class="form-control" id="exampleFormControlInput1" name="skill">
        </div>
        <div class="form-group">
            <label for="exampleFormControlInput2">Percent</label>
            <input type="text" class="form-control" id="exampleFormControlInput2" name="percent">
        </div>
        <div class="form-group">
            <label for="exampleFormControlSelect1">Type</label>
            <select name="type" class="form-control" id="exampleFormControlSelect1">
                <option value="prof">Prof</option>
                <option value="personal">Personal</option>
            </select>
        </div>
        <div class="form-group">
            <button class="btn btn-primary">Add</button>
        </div>
    </form>
</div>
</body>

</html>

