<?php

/**
 * Class DBConnector
 */
class DBConnector
{

    private const DNS = 'mysql:dbname=cv;host=127.0.0.1';
    private const USER = 'root';
    private const PASSWORD = 'root';

    /**
     * @var null
     */
    private static $instance = null;

    /**
     * @return PDO|null
     */
    public static function getInstance(): ?\PDO
    {
        if (!self::$instance) {
            self::$instance = new \PDO(self::DNS, self::USER, self::PASSWORD);
        }

        return self::$instance;
    }
}