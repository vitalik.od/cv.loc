<?php

include_once $_SERVER['DOCUMENT_ROOT'] . '/admin/skills/SkillsRepository.php';

$skillRepository = new SkillsRepository();
$skills = $skillRepository->getAllByType('pro');

?>

<!DOCTYPE html>
<html>
<head>
    <title>admin</title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
</head>

<body>
<nav class="navbar navbar-expand-lg navbar-light bg-light">
    <div class="collapse navbar-collapse" id="navbarNav">
        <ul class="navbar-nav">
            <li class="nav-item">
                <a class="nav-link" href="/admin/">Home <span class="sr-only">(current)</span></a>
            </li>
            <li class="nav-item active">
                <a class="nav-link" href="/admin/skills/pro">Prof skill</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="/admin/skills/per">Personal skill</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="/admin/works">Works</a>
            </li>
        </ul>
    </div>
</nav>
<table class="table table-dark">
    <thead>
    <tr>
        <th scope="col">#</th>
        <th scope="col">Skill</th>
        <th scope="col">Percent</th>
        <th scope="col">Type</th>
        <th scope="col"><a href="/admin/skills/pro/formInsert.php">Add</a></th>
    </tr>
    </thead>
    <tbody>
    <?php foreach ($skills as $skill): ?>
    <tr>
        <th scope="row"><?=$skill['id']?></th>
        <td><?=$skill['name']?></td>
        <td><?=$skill['level']?></td>
        <td><?=$skill['type']?></td>
        <td><a href="/admin/skills/pro/formUpdate.php?id=<?=$skill['id']?>">Edit</a></td>
        <td><a href="/admin/skills/pro/delete.php?id=<?=$skill['id']?>">Delete</a></td>
    </tr>
    <?php endforeach; ?>
    </tbody>
</table>
</body>

</html>
