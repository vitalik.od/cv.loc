<?php

include_once $_SERVER['DOCUMENT_ROOT'] . '/admin/DBConnector.php';

/**
 * Class SkillsRepository
 */
class SkillsRepository
{

    /**
     * @var PDO|null
     */
    private $connect;

    /**
     * SkillsRepository constructor.
     */
    public function __construct()
    {
        $this->connect = DBConnector::getInstance();
    }

    /**
     * @return array
     */
    public function getAll(): array
    {
        $sql = "select * from cv.skills ";
        $sth = $this->connect->prepare($sql);
        $sth->execute();

        return $sth->fetchAll(\PDO::FETCH_ASSOC);
    }

    /**
     * @param string $id
     * @return mixed
     */
    public function getById(string $id)
    {
        $sql = "select * from cv.skills where id = :id";
        $sth = $this->connect->prepare($sql);
        $sth->bindParam(':id', $id);
        $sth->execute();

        return $sth->fetch(\PDO::FETCH_ASSOC);
    }

    /**
     * @param string $type
     * @return array
     */
    public function getAllByType(string $type): array
    {
        $sql = "select * from cv.skills where type = :type";
        $sth = $this->connect->prepare($sql);
        $sth->bindParam(':type', $type);
        $sth->execute();

        return $sth->fetchAll(\PDO::FETCH_ASSOC);
    }

    /**
     * @param $params
     * @return bool
     */
    public function save(array $params): bool
    {
        $sql = <<< SQL
            INSERT INTO 
                skills (name, level, type ) 
            VALUES 
                   (:name, :level, :type );
SQL;

        /**
         * @var \PDOStatement $stmt
         */
        $stmt = $this->connect->prepare($sql);
        $stmt->bindParam(':name', $params['name']);
        $stmt->bindParam(':level', $params['level']);
        $stmt->bindParam(':type', $params['type']);

        return $stmt->execute();
    }

    /**
     * @param array $params
     * @return bool
     */
    public function update(array $params): bool
    {
        $sql = <<< SQL
            UPDATE 
                skills
             SET name = :name, level = :level, type = :type
            where id = :id
SQL;

        /**
         * @var \PDOStatement $stmt
         */
        $stmt = $this->connect->prepare($sql);
        $stmt->bindParam(':id', $params['id']);
        $stmt->bindParam(':name', $params['name']);
        $stmt->bindParam(':level', $params['level']);
        $stmt->bindParam(':type', $params['type']);

        return $stmt->execute();
    }

    /**
     * @param string $id
     * @return bool
     */
    public function delete(string $id): bool
    {
        $sql = <<< SQL
            DELETE FROM
                skills
            where id = :id
SQL;
        /**
         * @var \PDOStatement $stmt
         */
        $stmt = $this->connect->prepare($sql);
        $stmt->bindParam(':id', $id);

        return $stmt->execute();
    }
}