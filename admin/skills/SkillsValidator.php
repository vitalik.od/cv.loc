<?php

/**
 * Class SkillsValidator
 */
class SkillsValidator
{

    /**
     * @var array[]
     */
    private array $errors = [
        'name' => [],
        'level' => [],
        'type' => [],
    ];

    /**
     * @var bool
     */
    public bool $isValid = true;

    /**
     * @param array $params
     * @return bool
     */
    public function validate(array $params): bool
    {

        if (empty($params['name'])) {
            $this->errors['name'][] = "Skill must be not empty";
            $this->isValid = false;
        }

        if (!is_numeric($params['level'])) {
            $this->errors['level'][] = "Percent must be numeric";
            $this->isValid = false;
        }

        if (!in_array($params['type'], ['pro', 'per'])) {
            $this->errors['type'][] = "Type must be pro or per";
            $this->isValid = false;
        }

        return $this->isValid;
    }

    /**
     * @return array[]
     */
    public function getErrors(): array
    {
        return $this->errors;
    }
}