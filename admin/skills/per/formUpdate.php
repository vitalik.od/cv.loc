<?php

include_once $_SERVER['DOCUMENT_ROOT'] . '/admin/skills/SkillsRepository.php';

$skillRepository = new SkillsRepository();

$skill = $skillRepository->getById($_GET['id']);

?>

<!DOCTYPE html>
<html>
<head>
    <title>admin</title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
</head>

<body>
<div class="container">
    <form action="./update.php" method="post">
        <input type="hidden" name="id" value="<?=$_GET['id']?>"/>
        <div class="form-group">
            <label for="exampleFormControlInput1">Skill</label>
            <input type="text" class="form-control" id="exampleFormControlInput1" value="<?=$skill['name']?>" name="name">
        </div>
        <div class="form-group">
            <label for="exampleFormControlInput2">Percent</label>
            <input type="text" class="form-control" id="exampleFormControlInput2" value="<?=$skill['level']?>" name="level">
        </div>
        <div class="form-group">
            <label for="exampleFormControlSelect1">Type</label>
            <select name="type" required class="form-control" id="exampleFormControlSelect1">
	            <option value="" selected disabled hidden>Choose here</option>
                <option value="pro">Professional</option>
                <option value="per">Personal</option>
            </select>
        </div>
        <div class="form-group">
            <button class="btn btn-primary">Update</button>
        </div>
    </form>
</div>
</body>

</html>

