<?php

include_once $_SERVER['DOCUMENT_ROOT'] . '/admin/DBConnector.php';

/**
 * Class WorksRepository
 */
class EducationRepository
{

    /**
     * @var PDO|null
     */
    private $connect;

    /**
     * WorksRepository constructor.
     */
    public function __construct()
    {
        $this->connect = DBConnector::getInstance();
    }

    /**
     * @return array
     */
    public function getAll()
    {
        $sql = "select * from cv.education ";
        $sth = $this->connect->prepare($sql);
        $sth->execute();

        return $sth->fetchAll(\PDO::FETCH_ASSOC);
    }

    /**
     * @param string $id
     * @return mixed
     */
    public function getById(string $id)
    {
        $sql = "select * from cv.education where id = :id";
        $sth = $this->connect->prepare($sql);
        $sth->bindParam(':id', $id);
        $sth->execute();

        return $sth->fetch(\PDO::FETCH_ASSOC);
    }

    /**
     * @param $params
     * @return bool
     */
    public function save(array $params): bool
    {
        $sql = <<< SQL
            INSERT INTO 
                education (school, period, subject, url, details ) 
            VALUES 
                   (:school, :period , :subject, :url, :details );
SQL;

        /**
         * @var \PDOStatement $stmt
         */
        $stmt = $this->connect->prepare($sql);
        $stmt->bindParam(':school', $params['school']);
        $stmt->bindParam(':period', $params['period']);
        $stmt->bindParam(':subject', $params['subject']);
        $stmt->bindParam(':url', $params['url']);
        $stmt->bindParam(':details', $params['details']);

        return $stmt->execute();
    }

    /**
     * @param array $params
     * @return bool
     */
    public function update(array $params): bool
    {
        $sql = <<< SQL
            UPDATE  education
            SET     school = :school,
                    period=:period,
                    subject = :subject,
                    url = :url,
                    details = :details
            WHERE   id = :id
SQL;

        /**
         * @var \PDOStatement $stmt
         */
        $stmt = $this->connect->prepare($sql);
        $stmt->bindParam(':id', $params['id']);
        $stmt->bindParam(':school', $params['school']);
        $stmt->bindParam(':period', $params['period']);
        $stmt->bindParam(':subject', $params['subject']);
        $stmt->bindParam(':url', $params['url']);
        $stmt->bindParam(':details', $params['details']);

        return $stmt->execute();
    }

    /**
     * @param string $id
     * @return bool
     */
    public function delete(string $id): bool
    {
        $sql = <<< SQL
            DELETE FROM
                education
            where id = :id
SQL;
        /**
         * @var \PDOStatement $stmt
         */
        $stmt = $this->connect->prepare($sql);
        $stmt->bindParam(':id', $id);

        return $stmt->execute();
    }
}