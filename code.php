<?php

ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);

$var = rand(1, 2021);
$items = [
    [
        'title' => 'Product1',
        'sales' => 2021,
        'lastMonthSales' => 1200,
        'views' => $var,
        'price' => 115.5,
        'priceNetto' => 100,
        'stock' => 3,
        'isEnable' => 1,
        'isEnableForeignSale' => 0,
    ],
    [
        'title' => 'Product2',
        'sales' => 2021,
        'lastMonthSales' => 100,
        'views' => $var,
        'price' => 115.5,
        'priceNetto' => 100,
        'stock' => 3,
        'isEnable' => 1,
        'isEnableForeignSale' => 0,
    ],
    [
        'title' => 'Product3',
        'sales' => 2021,
        'lastMonthSales' => 1000,
        'views' => 2021,
        'price' => 115.5,
        'priceNetto' => 100,
        'stock' => 3,
        'isEnable' => 1,
        'isEnableForeignSale' => 1,
    ],
    [
        'title' => 'Product4',
        'sales' => 2021,
        'lastMonthSales' => 1300,
        'views' => $var,
        'price' => 115.5,
        'priceNetto' => 100,
        'stock' => 1,
        'isEnable' => 0,
        'isEnableForeignSale' => 1,
    ],
    [
        'title' => 'Product5',
        'sales' => 2021,
        'lastMonthSales' => 1000,
        'views' => $var,
        'price' => 115.5,
        'priceNetto' => 100,
        'stock' => 6,
        'isEnable' => 1,
        'isEnableForeignSale' => 1,
    ]
];

$totalViews = 0;

for ($i = 0, $j = 0, $dataCount = count($items); $i < $dataCount; $i++) {
    if ($items[$i]['isEnableForeignSale'] > 0) {
        $totalViews = $totalViews + $items[$i]['views'];
        $j++;
    }
}
echo "Found: " . $j . " items<br>Total views: " . $totalViews;
?>